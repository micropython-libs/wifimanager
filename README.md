# wifimanager

## Supported settings

- "wifi_cred" - list of dicts {"SSID": "my_ssid", "PASSWD": "passwd"}
- "wifi_url_check" - list of urls used to test connection

# TODO: Power management
# TODO: Detect captive portal

## Features

Python package for managing wifi connections.
Functions:
- Scan for available networks
- Select network with known credentials
- Connect to network
- Check if connection is active
- Reconnect if connection failed
- Cycle over available networks if some fail