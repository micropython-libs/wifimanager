#!/usr/bin/env python

from distutils.core import setup

setup(name='wifimanager',
      version='0.1.0',
      description='Wifi manager for asyncio micropython project',
      author='Jan Klusáček',
      author_email='honza.klugmail.com',
      url='https://gitlab.com/users/honza.klu',
      packages=['wifimanager'],
     )