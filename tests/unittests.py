import _thread
import time
import asyncio
import logging
import json

import unittest

import wifimanager

root_logger = logging.getLogger()
root_logger.setLevel(logging.DEBUG)

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

loop = asyncio.get_event_loop()


class PrintHandler(logging.Handler):
    def emit(self, record):
        # Format the log message
        log_message = self.format(record)
        # Print the log message
        print(log_message)


# Add the custom PrintHandler
print_handler = PrintHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
print_handler.setFormatter(formatter)
root_logger.addHandler(print_handler)

env = json.load(open("env.json", "rb"))


def destroy_wifi_manager():
    if wifimanager._wifi_manager is not None:
        asyncio.run(wifimanager._wifi_manager.stop())
        wifimanager._wifi_manager = None


class TestSubtest(unittest.TestCase):
    def test_factory(self):
        destroy_wifi_manager()
        wifi_manager = wifimanager.get_wifimanager()

        async def test_coro():
            await wifi_manager.init()

        ret = loop.run_until_complete(test_coro())

        self.assertEqual(ret, None)
        self.assertEqual(wifi_manager.get_status(), "DISABLED")

    def test_basic_connect_no_network(self):
        """ Test what happen if there is no known network

        """
        destroy_wifi_manager()

        settings = {'wifi_cred': [{'SSID': 'default', 'PASSWD': 'None'}]}
        wifi_manager = wifimanager.get_wifimanager(settings=settings)

        async def test_coro():
            await wifi_manager.init()
            # Initially it is disabled
            self.assertEqual(wifi_manager.get_status(), "DISABLED")
            await asyncio.sleep(10.0)
            self.assertEqual(wifi_manager.get_status(), "DISABLED")
            # wi-fi is not connected because no existing wi-fi is available
            wifi_manager.set_status("CONNECTING")  # TODO: Do not use this
            await asyncio.sleep(10.0)
            self.assertEqual(wifi_manager.get_status(), "FAILED")

        ret = asyncio.run(test_coro())

    def test_basic_connect(self):
        """ Test what happen if there is no known network

        """
        destroy_wifi_manager()

        settings = {'wifi_cred': [{'SSID': env["WIFI_SSID"], 'PASSWD': env["WIFI_PASSWD"]}]}
        wifi_manager = wifimanager.get_wifimanager(settings=settings)

        async def test_coro():
            await wifi_manager.init()
            # Initially it is disabled
            self.assertEqual(wifi_manager.get_status(), "DISABLED")
            await asyncio.sleep(10.0)
            self.assertEqual(wifi_manager.get_status(), "DISABLED")
            # WIFI is not connected because no existing wifi is available
            #wifi_manager.set_status("CONNECTING")
            wifi_manager.connect()
            self.assertEqual(wifi_manager.get_status(), "CONNECTING") # Connection take some time
            await wifi_manager.wait_connect()
            self.assertEqual(wifi_manager.get_status(), "CONNECTED")

        ret = asyncio.run(test_coro())

    def test_basic_reconnect(self):
        """ Test what happen if there is no known network

        """
        destroy_wifi_manager()

        settings = {'wifi_cred': [{'SSID': env["WIFI_SSID"], 'PASSWD': env["WIFI_PASSWD"]}]}
        wifi_manager = wifimanager.get_wifimanager(settings=settings)

        async def test_coro():
            await wifi_manager.init()
            # Initially it is disabled
            self.assertEqual(wifi_manager.get_status(), "DISABLED")
            await asyncio.sleep(10.0)
            self.assertEqual(wifi_manager.get_status(), "DISABLED")
            # WIFI is not connected because no existing wifi is available
            #wifi_manager.set_status("CONNECTING")
            wifi_manager.connect()
            self.assertEqual(wifi_manager.get_status(), "CONNECTING") # Connection take some time
            await wifi_manager.wait_connect()
            self.assertEqual(wifi_manager.get_status(), "CONNECTED")
            wifi_manager.disconnect()
            await asyncio.sleep(5)
            self.assertEqual(wifi_manager.get_status(), "DISABLED")
            await wifi_manager.wait_connect()
            self.assertEqual(wifi_manager.get_status(), "CONNECTED")

        ret = asyncio.run(test_coro())
