import network
import ujson as json
import uasyncio as asyncio
import uaiohttpclient as aiohttp
import time

import asynchelper

loop = asyncio.get_event_loop()
helper = asynchelper.getAyncThreadHelper()

import logging

logger = logging.getLogger(__name__)

cred = {}

DEFAULT_WIFI_CRED = {"SSID": "FreeWifi", "PASSWD": ""}
INTERFACE_DOWN_INTERVAL = 1000  # ms
INTERFACE_UP_INTERVAL = 1000  # ms

_wifi_manager = None

sta_if = network.WLAN(network.STA_IF) # TODO: Move this?

def get_wifimanager(*args, **kwargs):
    global _wifi_manager
    if _wifi_manager is None:
        _wifi_manager = WifiManager(*args, **kwargs)
    return _wifi_manager


# TODO: Cycle wifi if previous one failed
class WifiManager:
    WIFI_STATES = {"DISABLED", "CONNECTING", "CONNECTED", "FAILED"}
    def __init__(self, *, check_period=60, start_connection_timeout=30, # TODO: Document these parameters
                 settings=None):
        self.auto_reconnect = True
        self._status = "DISABLED"  # DISABLED, CONNECTING, CONNECTED, FAILED
        self.check_period = int(check_period * 1000)
        self.start_connection_timeout = start_connection_timeout
        # self.start_connection_timeout = start_connection_timeout*1000
        self.settings = settings or {}

        self.background_task = None
        self.background_sleep_event = asyncio.Event()
        self.connected_event = asyncio.Event()
        self.current_credentials = None # Currently used credentials
        self.scanned_ssids = []
        self.failed_ssids = {} # Key should be tuple (ssid, passwd)
        self.interface_up = False

        self._on_connect = []
        self._on_disconnect = []
        self._on_fail = []

    async def init(self):
        if self.background_task is not None:
            raise ValueError("WifiManager is already initialized")
        self.background_task = loop.create_task(self._backgroud_task_coro())
        logger.info("WifiManager initialized")

    async def stop(self):
        logger.info("Stopping WifiManager")
        if self.background_task is not None:
            self.background_task.cancel()
            try:
                await self.background_task
            except asyncio.CancelledError as e:
                pass
            self.background_task = None

    def connect(self):
        if self.get_status() not in {"CONNECTED", "CONNECTING"}:
            self.set_status("CONNECTING")

    async def wait_connect(self):
        self.connect()
        if self.get_status() != "CONNECTED":
            self.connected_event.clear()
            await self.connected_event.wait()
        self.connected_event.clear()

    def disconnect(self):
        self.set_status("DISABLED")

    def get_status(self):
        return self._status

    def set_status(self, value):
        if value not in self.WIFI_STATES:
            raise ValueError(f"value is not valid WifiManager status")
        if self._status == value:
            return
        logger.debug(f"Changing status from {self._status} to {value}")
        if value == "FAILED" and self.current_credentials is not None:
            self.failed_ssids[(self.current_credentials["SSID"], self.current_credentials["PASSWD"])] = time.ticks_ms()
            logger.info(f"Connection to {self.current_credentials} failed")
        # Wake up if we try to connect
        if value == "CONNECTING":
            self.background_sleep_event.set()
        if value == "CONNECTED":
            self.connected_event.set()
            for cb in self._on_connect:
                cb()
        if value == "FAILED":
            for cb in self._on_fail:
                cb()
        if value in {"FAILED", "DISABLED"}:
            for cb in self._on_disconnect:
                cb()
        self._status = value

    def register_on_connect(self, callback):
        if callback not in self._on_connect:
            self._on_connect.append(callback)

    def unregister_on_connect(self, callback):
        if callback in self._on_connect:
            self._on_connect.remove(callback)

    def register_on_disconnect(self, callback):
        if callback not in self._on_disconnect:
            self._on_disconnect.append(callback)

    def unregister_on_disconnect(self, callback):
        if callback in self._on_disconnect:
            self._on_disconnect.remove(callback)

    def register_on_fail(self, callback):
        if callback not in self._on_fail:
            self._on_fail.append(callback)

    def unregister_on_fail(self, callback):
        if callback in self._on_fail:
            self._on_fail.remove(callback)

    def _enable_interface(self):
        logger.debug("Enable network interface")
        sta_if.active(True)
        # Set powermode
        try:
            # sta_if.config(pm=sta_if.PM_NONE)  # TODO: Make this configurable?
            pass
        except Exception as e:
            logger.error(f"Setting network power mode not supported ({e})")
        self.interface_up = True

    def _disable_interface(self):
        logger.debug("Disable network interface")
        sta_if.active(False)
        self.interface_up = False

    async def _scan(self):
        def test():
            time.sleep(2)
            return []

        self.scanned_ssids = await helper.run_in_executor(sta_if.scan)
        return self.scanned_ssids

    async def _check_status(self):
        # TODO: Check ip address?
        if "wifi_checks" in self.settings:
            urls = self.settings["wifi_url_check"]
        else:
            urls = ["http://google.com"]
        for url in urls:
            try:
                resp = await aiohttp.request("GET", url)
                logger.debug(f"Checking {url} returned {resp.status}")
                if 200 <= resp.status <= 299:
                    if self.get_status() is not "DISABLED": # Prevent setting to connected when already disconnecting
                        self.set_status("CONNECTED")
                    return
                else:
                    logger.debug(f"Checking connection using {url} failed with {resp._status}")
            except OSError as e:
                logger.debug(f"Connection test raised {e} for {url}")
        else:
            self.set_status("FAILED")
            if self.current_credentials is not None:
                cred = self.current_credentials
                self.failed_ssids[(cred["SSID"], cred["PASSWD"])] = time.ticks_ms()

    def _find_ssid(self):
        best_cred = None
        best_failed_time = time.ticks_ms()

        creds = self.settings.get("wifi_cred", DEFAULT_WIFI_CRED)
        for cred in creds:
            ssid = cred["SSID"]
            passwd = cred["PASSWD"]
            if ssid not in [scan_rec[0].decode('ascii') for scan_rec in self.scanned_ssids]:
                continue
            last_fail = self.failed_ssids.get((ssid, passwd), 0)
            if last_fail < best_failed_time:
                best_cred = cred
                best_failed_time = last_fail
        if best_cred is None:
            logger.debug(f"No known wifi found")
        return best_cred

    async def _connect(self):
        if self.interface_up:
            self._disable_interface()
            await asyncio.sleep_ms(INTERFACE_DOWN_INTERVAL)
        self._enable_interface()
        await asyncio.sleep_ms(INTERFACE_UP_INTERVAL)
        await self._scan()
        # Find known wifi
        self.current_credentials = self._find_ssid()
        # Connect
        if self.current_credentials is None:
            return
        logger.info(f"Connecting to {self.current_credentials}")
        print(f'sta_if.connect({self.current_credentials["SSID"]}, {self.current_credentials["PASSWD"]})')
        sta_if.connect(self.current_credentials["SSID"], self.current_credentials["PASSWD"])
        start = time.ticks_ms()
        while time.ticks_ms() - start < self.start_connection_timeout*1000:
            logger.debug(f'{sta_if.ipconfig("addr4")}')
            if not sta_if.isconnected():
                logger.debug("Wifi is not connected yet")
            else:
                break
            await asyncio.sleep_ms(1000)
        # Check if connection was successful - Will be done automatically later
        # await self._check_status()

    async def _backgroud_task_coro(self):
        while True:
            if self.get_status() == "CONNECTED":
                await asyncio.wait_for_ms(self.background_sleep_event.wait(), self.check_period)
                self.background_sleep_event.clear()
            if self.get_status() in {"CONNECTING", "FAILED"}:
                await self._connect()
            if self.get_status() != "DISABLED":
                await self._check_status()
            if self.get_status() == "DISABLED":
                self._disable_interface()
                await self.background_sleep_event.wait()
                self.background_sleep_event.clear()
